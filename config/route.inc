<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 12:46
 */

$route = [
    'index' => array(
        'controller' => 'Controller\IndexController',
        'defaults' => array(
            'action' => array(
                'name' => 'index'
            )
        )
    ),
    'login' => array(
        'controller' => 'Controller\LoginController',
        'defaults' => array(
            'action' => array(
                'name' => 'index'
            )
        )
    ),
    'manage' => array(
        'controller' => 'Controller\ManageController',
        'defaults' => array(
            'action' => array(
                'name' => 'index'
            )
        ),
        'variables' => array(
            'actions' => array(
                'add' => array(
                    'name' => 'add'
                ),
                'edit' => array(
                    'name' => 'edit',
                    'attrs' => array(
                        'id' => "/^[0-9]+$/"
                    )
                )
            )
        )
    )
];