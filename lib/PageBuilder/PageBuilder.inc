<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 13.06.2016
 * Time: 13:52
 */

namespace PageBuilder;

class PageBuilder
{
    private $controller = null;  // Obiekt kontrolera
    private $action = null;      // Akcja String
    private $attrs = null;       // atrybuty akcji tablica stringów albo null

    private $route;


    /**
     * PageBuilder constructor.
     * @param $route
     */
    function __construct($route)
    {
        $this->route = $route;

        $urlTable = explode("/",$_SERVER["REQUEST_URI"]);
        unset($urlTable["0"]);

        if(isset($urlTable[3])){
            die("Taka strona nie istnieje");
        }

        $tmp = array();

        if(isset($urlTable[2]))
            $tmp = explode('?', $urlTable[2]);

        if(sizeof($tmp) > 2)
            die ("Zabroniony znak w przesyłanym atrybucie");

        $urlController = 'index';
        if($urlTable[1] != "")
            $urlController = $urlTable[1];
        $this->controller = $this->getController($urlController);

        $urlAction = null;
        if(isset($tmp[0])){
            $urlAction = $tmp[0];
        }

        $this->action = $this->getAction($urlAction, $urlController);

        $urlAttrs = null;
        if(isset($tmp[1])){
            $urlAttrs = $tmp[1];
        }
        $this->attrs = $this->getActionAttribute($urlAttrs, $urlAction, $urlController);
    }

    /**
     * @param $urlController
     * @return String
     */
    private function getController($urlController)
    {
        if(array_key_exists($urlController,$this->route)) {
            $str = $this->route[$urlController]['controller'];
            return  new $str;
        }
        die("Taka strona nie istnieje");
    }


    /**
     * @param $urlAction
     * @param $urlController
     * @return String
     */
    private function getAction($urlAction, $urlController)
    {
        if($urlAction == '' || !$urlAction){
            return $this->route[$urlController]['defaults']['action']['name'];
        }elseif($urlAction && sizeof($urlAction) > 0){
            if(
                isset($this->route[$urlController]['variables']['actions'][$urlAction]) &&
                $urlAction == $this->route[$urlController]['variables']['actions'][$urlAction]['name']
            ){
                return $urlAction;
            }else{
                die("Nie ma takiej strony");
            }
        }else{
            die("Nie ma takiej strony");
        }

    }

    /**
     * @param $attrs
     * @param $urlAction
     * @param $urlController
     * @return array<String>
     */
    private function getActionAttribute($attrs, $urlAction, $urlController)
    {
        if($attrs && isset($this->route[$urlController]['variables']['actions'][$urlAction]['attrs'])){

            $attributes = $this->explodeArrayOfStrings(explode('&', $attrs));
            $regExps = $this->route[$urlController]['variables']['actions'][$urlAction]['attrs'];

            foreach($attributes as $key=>$value){
                if(array_key_exists($key, $regExps)){
                    if(!preg_match($regExps[$key], $value)){
                        die("Atrybut :".$key." nie przeszdł waliacji");
                    }
                }else{
                    die("Atrybut :|". $key ."| nie został przewidziany dla tego zapytania");
                }
            }

            return $attributes;

        }elseif($attrs && isset($this->route[$urlController]['defaults']['action']['name'][$urlAction])){
            return $this->route[$urlController]['defaults']['action']['attrs'];
        }else{
            return null;
        }
    }

    private function explodeArrayOfStrings($array){
        $attrs = array();
        foreach($array as $value){
            $tmp = explode("=", $value);

            $attrs[$tmp[0]] = $tmp[1];
        }
        return $attrs;
    }

    public function buildPage(){

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $action = $this->action."Action";
            die( $this->controller->$action($this->attrs));
        }

        $action = $this->action."Action";
        $this->controller->$action($this->attrs);



    }
}