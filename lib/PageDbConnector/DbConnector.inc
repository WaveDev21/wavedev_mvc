<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 16:15
 */

namespace PageDbConnector;

use PDO;

class DbConnector
{
    public $connection = null;

    public function __construct()
    {
        require_once "../config/db.inc";

        try {
            $this->connection = new PDO("mysql:host=".__HOST_DB__.";dbname=".__NAME_DB__.";",__USER_DB__, __PASS_DB__);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch(PDOException $e)
        {
            die("Connection failed: " . $e->getMessage());
        }

    }

    public function disconnect(){
        $this->connection = null;
    }
}