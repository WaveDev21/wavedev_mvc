<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 13:14
 */

namespace PageView;


class View
{

    /**
     * @param null $dataTable
     * @return mixed
     */
    public static function getView($dataTable = null){

        $function = debug_backtrace()[1]['function'];
        $function = str_replace("Action", '', $function);

        $class = debug_backtrace()[1]['class'];
        $class = explode('\\',$class);
        $class = str_replace("Controller", "", $class[1]);


        ?>
        <html>
        <head>
            <?php require_once "../head.php"?>
        </head>
        <body>
        <header>
            <?php require_once "../header.php"?>
            <?php require_once "../nav.php"?>
        </header>
        <main>
            <?php
            require_once "../src/view/$class/$function.php";
            ?>
        </main>
        <footer>
            <?php require_once "../footer.php"?>
        </footer>
        </body>
        </html>

        <?php



    }


}