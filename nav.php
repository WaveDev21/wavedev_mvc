<?php

if(isset($_SESSION["user"])){
    ?>
    <nav class=" container-fluid">
        <ul class="nav nav-justified ">
            <li><a href="/">Główna</a></li>
            <li><a href="#">O mnie</a></li>
            <li><a href="#">Kontakt</a></li>
            <li><a href="/manage">Zarządzaj</a></li>
            <li><a href="public/index.php"><span class="glyphicon glyphicon-log-out"></span> Witaj <?php echo(" ".$_SESSION['user']['name']); ?> </a> </li>
        </ul>
    </nav>
    <?php
    if(isset($dataTable['err'])) {
        ?>
        <div class="container-fluid bg-info text-center "><h4><?php echo $dataTable['err']; ?></h4></div>
        <?php
    }
}else{
    ?>
    <nav class=" container-fluid">
        <ul class="nav nav-justified ">
            <li><a href="/">Główna</a></li>
            <li><a href="#">O mnie</a></li>
            <li><a href="#">Kontakt</a></li>
            <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Zaloguj </a> </li>
        </ul>
    </nav>
    <?php
        if(isset($dataTable['err'])) {
            ?>
            <div class="container-fluid bg-info text-center "><h4><?php echo $dataTable['err']; ?></h4></div>
            <?php
        }
}