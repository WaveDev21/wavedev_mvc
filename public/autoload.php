<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 13.06.2016
 * Time: 10:44
 */

function __autoload($className){

    $className = str_replace('\\', '/', $className);
    $directory = explode(',',__DIR_LIB__);

    foreach($directory as $value){
        $file = "./../".$value."/".$className.__SUFIX_CLASS__;
        if(is_readable($file)){
            require_once $file;
        }
    }

}