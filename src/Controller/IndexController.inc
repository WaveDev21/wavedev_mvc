<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 13.06.2016
 * Time: 14:21
 */

namespace Controller;


use PageView\View;

class IndexController
{
    public function __construct()
    {
    }

    public function indexAction(){

        View::getView();
    }
}