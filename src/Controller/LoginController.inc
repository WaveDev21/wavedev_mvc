<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 13.06.2016
 * Time: 14:21
 */

namespace Controller;


use PageView\View;
use Service\Login;

class LoginController
{
    private $service = null;
    public function __construct()
    {
        $this->service = new Login();
    }

    public function indexAction(){
        if(isset($_POST['login']) && isset($_POST['pass'])){
            if($user = $this->service->checkUser($_POST['login'],$_POST['pass'])){
                $_SESSION["user"] = $user;
                unset($_POST);
                header('Location: index');

            }else{
                $_POST['err'] = "Nie właściwe hasło lub login";
                return View::getView($_POST);
            }

        }else{
            $err = null;
            if(isset($_SESSION['user'])){
                $err = ["err"=>"Zostałeś wylogowany"];
            }
            unset($_SESSION['user']);
            session_destroy();
            return View::getView($err);
        }

    }
}