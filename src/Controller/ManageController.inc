<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 20:00
 */

namespace Controller;


use PageView\View;
use Service\ManageUsers;

class ManageController
{
    private $userService = null;
    public function __construct()
    {
        $this->userService = new ManageUsers();
    }

    public function indexAction(){

        View::getView($this->userService->getAll());
    }

    public function addAction(){
        die("add");
    }

    public function editAction($attrs){

        View::getView(["user" =>$this->userService->getUser($attrs['id'])]);
    }

}