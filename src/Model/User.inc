<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-09
 * Time: 16:13
 */

namespace Model;


class User
{
    public $id;
    public $login;
    public $email;
    public $pass;

    public function __construct($id, $login, $pass, $email)
    {
        $this->id = $id;
        $this->login = $login;
        $this->pass = $pass;
        $this->email = $email;
    }

    
}