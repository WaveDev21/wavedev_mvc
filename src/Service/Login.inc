<?php

/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-09
 * Time: 16:33
 */
namespace Service;
use Model\User;
use PageDbConnector\DbConnector;
use PDOException;
use PDO;

class Login
{
    private $connector = null;
    public function __construct()
    {
        $this->connector = new DbConnector();
    }

    public function isUser($tab, $email, $pass){
        foreach ($tab as $user){
            if($user->email == $email && $user->pass == $pass){
                return true;
            }
        }
        return false;
    }

    public function getUser($tab, $email){
        foreach ($tab as $user){
            if($user->email == $email){
                return $user;
            }
        }
        return false;
    }

    public function addUser($tab, $email, $pass){
        array_push($tab, new User($email, $pass));
        return $tab;
    }

    public function deleteUser($tab, $email){
        foreach ($tab as $key=>$user){
            if($user->email == $email){
                unset($tab[$key]);
                return $tab;
            }
        }
    }

    public function changePass($tab, $email, $pass){
        foreach ($tab as $user){
            if($user->email == $email){
                $user->pass = $pass;
                return $tab;
            }
        }
    }

    public function getPass($tab, $email){
        foreach ($tab as $user){
            if($user->email == $email){
                return "Twoje hasło to ".$user->pass;
            }
        }
        return "Nie ma takiego żytkownika";
    }

    public function checkUser($login, $pass){

        try{
            $stmt = $this->connector->connection->prepare('SELECT login, pass, email FROM t_users WHERE login = :login');
            $stmt->bindParam(':login', $login);
            $stmt->execute();

            foreach($stmt->fetchAll() as $row){
                if($row['pass'] == $pass){
                    $this->connector->disconnect();
                    return array('name' => $login, 'email' => $row['email']);
                }
            }

        }catch (\PDOException $e){
            die ("Error: ".$e->getMessage());
        }

        $this->connector->disconnect();
        return null;
    }
}