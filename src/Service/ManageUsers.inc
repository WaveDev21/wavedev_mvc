<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 20:09
 */

namespace Service;


use Model\User;
use PageDbConnector\DbConnector;

class ManageUsers
{
    private $connector = null;
    public function __construct()
    {
        $this->connector = new DbConnector();
    }

    public function getAll(){

        try{
            $stmt = $this->connector->connection->prepare('SELECT id, login, pass, email FROM t_users');
            $stmt->execute();

            $table = array();

            foreach($stmt->fetchAll() as $row){

                array_push($table, new User(
                    $row['id'],
                    $row['login'],
                    $row['pass'],
                    $row['email']
                ));
            }

            $this->connector->disconnect();
            return $table;

        }catch (\PDOException $e){
            die ("Error: ".$e->getMessage());
        }

    }

    public function getUser($id){

        try{
            $stmt = $this->connector->connection->prepare('SELECT id, login, pass, email FROM t_users WHERE id = :id');
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            if($row = $stmt->fetchAll()[0]) {

                $this->connector->disconnect();

                return new User(
                    $row['id'],
                    $row['login'],
                    $row['pass'],
                    $row['email']
                );
            }

        }catch (\PDOException $e){
            die ("Error: ".$e->getMessage());
        }
        $this->connector->disconnect();
        return null;
    }
}