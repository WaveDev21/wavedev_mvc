<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 12:36
 */

?>
<div class="col-lg-3"></div>
<div class="container col-lg-6 text-center">
    <div class="form-login-padding col-lg-12">
        <form class="form col-lg-12" method="post" action="login">
            <div class="form-inline form-group">
                <label  for="name">Login:</label>
                <input type="text" class="form-control" value="<?php if(isset($dataTable['login'])) echo $dataTable['login']; ?>" id="name" name="login" placeholder="Podaj login">
            </div>
            <div class="form-inline form-group">
                <label  for="pass">Hasło:</label>
                <input type="password" class="form-control" value="<?php if(isset($dataTable['pass'])) echo $dataTable['pass']; ?>" id="pass" name="pass" placeholder="Podaj hasło">
            </div>
            <div class="form-group text-right">
                <input type="submit"  class="form-submit btn btn-primary">
            </div>
        </form>
    </div>
</div>


