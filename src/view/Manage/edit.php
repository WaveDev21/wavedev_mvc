<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 22:16
 */
?>

<div class="col-lg-3"></div>
<div class="container col-lg-6 text-center">
    <div class="form-login-padding col-lg-12">
        <form class="form col-lg-12" method="post" action="/manage/edit?id=<?php echo $dataTable['user']->id; ?>">
            <div class="form-inline form-group">
                <label  for="login">Login:</label>
                <input type="text" class="form-control" value="<?php echo $dataTable['user']->login; ?>" id="login" name="login" placeholder="Podaj login">
            </div>
            <div class="form-inline form-group">
                <label  for="email">Email:</label>
                <input type="text" class="form-control" value="<?php echo $dataTable['user']->email; ?>" id="email" name="email" placeholder="Podaj hasło">
            </div>
            <div class="form-group text-right">
                <input type="submit" value="Aktualizuj" class="form-submit btn btn-primary">
            </div>
        </form>
    </div>
</div>
