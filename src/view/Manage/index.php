<?php
/**
 * Created by PhpStorm.
 * User: Wave
 * Date: 14.06.2016
 * Time: 20:42
 */
?>

<div class="container-fluid">
    <div class="container">
        <h1>Lista użytkowników</h1>
        <a class="btn btn-primary" href="manage/add">Dodaj nowego</a>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Login</th>
                    <th>Email</th>
                    <th>Opperations</th>
                </tr>
                <?php
                    foreach($dataTable as $key=>$user){
                        echo '<tr>';
                        echo '<td>'.$user->login.'</td>';
                        echo '<td>'.$user->email.'</td>';
                        echo '<td>'
                            .'<a href="/manage/delete?id='.$user->id.'" class="glyphicon glyphicon-remove">Delete</a> '
                            .'<a href="/manage/edit?id='.$user->id.'" class="glyphicon glyphicon-edit">Edit</a> '
                            .'<a href="/manage/display?id='.$user->id.'" class="glyphicon glyphicon-arrow-right">Show</a>'
                            .'</td>';
                        echo '</tr>';
                    }
                ?>
            </thead>
        </table>
    </div>
</div>